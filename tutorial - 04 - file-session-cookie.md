## Kiểm tra sự tồn tại của tập tin/thư mục

- file_exists(\$file_name) dùng để kiểm tra sự tồn tại của tập tin/thư mục cần kiểm tra
  - \$file_name là đường dẫn của tập tin hay thư mục
  - true => exists, false => not exists
- Các hàm lấy thông số của tập tin/thư mục
  - filetype($file_name) trả về kiểu của $file_name (là tập tin hay thư mục)
  - filesize($file_name) trả về dung lượng $file_name (byte)
  - is_readable(\$file_name) kiểm tra quyền đọc
  - is_writeable(\$file_name) kiểm tra quyền ghi
  - is_executable(\$file_name) kiểm tra quyền thực thi
- Các hàm lấy thông tin từ đường dẫn
  - basename($path) trả về tên của tập tin từ đường dẫn $path
  - dirname($path) trả về tên thư mục tại đường dẫn $path
  - pathinfo($path, $options) trả về một mảng chứa thông tin của đường dẫn \$path bao gồm
    - dirname
    - basename
    - extention
- Các hàm đọc tập tin
  - file(\$file_name) đọc tập tin thành một mảng. Mỗi dòng là 1 phần tử (read file line by line)
  - file_get_contents(\$file_name) đọc tập tin thành một chuỗi
- Các hàm ghi nội dung file
  - file_put_contents($file_name,$data,$mod) ghi nội dung $data vào \$file_name
  - \$mode = FILE_APPEND sẽ giữ lại nội dung file cũ và ghi data vào cuối tập tin
  - Nêu ghi thành công sẽ trả kết là là độ dài của \$data, ghi thất bại trả về false
- Sao chép tập tin
  - copy(\$source_file, destination_file) trả về true nếu coppy thành công
- Quyền trong tập tin
  - xyz đại diện cho User/Group/Guest
  - Giá trị các quyền 
    - Read = 4
    - Write = 2
    - Execute = 1
    - 7 = 4+1+2 full quyền
    - 6 = 4+2 quyền đọc, ghi
    - 5 = 4+1 quyền đọc, thực thi
- Tạo, xóa, cấp quyền cho thư mục
  - mkdir($path,$mode) tạo thư mục tại đường dẫn $path với quyền $mode
  - rmdir($path) xóa thư mục ở đường dẫn $path
  - file_perms($dir_name) xe quyền của thư mục $dir_name
  - chmod($dir_name,$mode) cấp quyền cho thư mục $dir_name 
  - $mode có 4 chữ số bắt đầu với 0: 0755
- glob($pattern) liệt kê danh sách tệp và thư mục match với $patttern trong thư mục hiện hành, truyền tham số thứ 2 là GLOB_ONLYDIR để chỉ trả về thư mục
- parse_ini_file($file_name) trả về mảng là các phần tử của file ini đọc được, truyền tham số thứ 2 là TRUE để trả về array bao gồm session của file ini
- Các hàm thao tác với thư mục
  - getcwd() trả về thư mục hiện hành
  - realpath() trả về đường dẫn tuyệt đối của $path
  - chdir() thay đổi thư mục hiện tại
  - dir() mở thư mục để thao tác với thư mục (read, rewind, close)
  - opendir() mở 1 thư mục lên để thao tác
  - closedir() đóng thư mục được mở bởi opendir()
  - scandir() lấy danh sách các tập tin và thư mục của đường dẫn $path

  ## Upload file
  - Các tham số file upload
    -$FILE['name-of-input-file']['name']: Tên file
    -$FILE['name-of-input-file']['size']: Kích thước file
    -$FILE['name-of-input-file']['type']: Kiểu file
    -$FILE['name-of-input-file']['tmp_name']: Tên thư mục tạm trên server chứa file
    -$FILE['name-of-input-file']['error']: Lỗi khi upload file
- move_upload_file($filename,$destionation) để upload tập tin $filename vào vị trí $destination, có thể dùng coppy để thực hiện việc tương tự

## Session

- Mỗi session được cấp 1 ID khác nhau và nội dung được lưu trong thư mục được thiết lập ở file php.ini với tham số session.save_path.
- Session hoạt động bằng cách tạo 1 chuỗi Unique (uid) cho từng vistore và chưa thông tin đó dựa trên ID đó.
- Khởi tạo session: session_start() - Hàm này phải được đặt trước <html>
- Cập nhật thông tin session thông qua biến \$\_SESSION
- Xóa sesstion: session_unset() hoặc session_detroy()
- Session có thể lưu những giá trị gì:

  - Biến
  - Mảng
  - Hàm
  - File
  - Hình ảnh

## Cookie

- Khác với session, cookie được lưu ở trình duyệt người dùng.
- Tạo cookie: setcookie($name,$value,$time)
- Truy cập cookie qua biến _COOKIE
- Hủy cookie: setcookie($name,$value,time()-3600)
