<?php
$menu = array();
$menu[] = array("id" => 1, "name" => "Phone", "parents" => 0);
$menu[] = array("id" => 2, "name" => "Tablet", "parents" => 0);
$menu[] = array("id" => 3, "name" => "Computer", "parents" => 0);
$menu[] = array("id" => 4, "name" => "HP", "parents" => 3);
$menu[] = array("id" => 5, "name" => "Dell", "parents" => 3);
$menu[] = array("id" => 6, "name" => "Monitor", "parents" => 5);
$menu[] = array("id" => 7, "name" => "CPU", "parents" => 5);
$menu[] = array("id" => 8, "name" => "SamSung", "parents" => 1);
$menu[] = array("id" => 9, "name" => "iPhone", "parents" => 1);

function recursive($source, $parents, $level, &$menuTree)
{
    if (count($source) > 0) {
        foreach ($source as $key => $value) {
            if ($value['parents'] == $parents) {
                $value['level'] = $level;
                $menuTree[] = $value;
                unset($source[$key]);
                $newParents = $value['id'];
                recursive($source, $newParents, $level+1, $menuTree);
            }
        }
    }
}
recursive($menu, 0, 1, $menuTree);

foreach ($menuTree as $key => $value) {
    if ($value['level'] == 1) {
        echo '<div style="border: 1px solid #ccc;">+' . $value['name'] . '</div>';
    } else {
        $padding = ($value['level'] - 1) * 20;
        $padding = 'padding-left: ' . $padding . 'px';
        echo '<div style="border: 1px solid #ccc;' . $padding . '">-' . $value['name'] . '</div>';
    }

}
