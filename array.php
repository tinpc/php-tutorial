<h2>Tổng quan</h2>
<?php
/**
 *@Khai báo mảng
 */
$arr = array();
/**
 *@Thêm phần tử vào mảng
 */
$arr[] = 'item 1';
$arr[] = 'item 2';
$arr[] = 'item 3';
$arr[] = 'item 4';
$arr[] = 'item 5';
/**
 *@Đếm số phần tử trong mảng
 */
$length = count($arr);
echo $length;
/**
 *@Kiểm tra mảng có rỗng không
 */
$arr_is_empty = empty($arr) ? 'Không rỗng' : 'Rỗng';
echo $arr_is_empty;
/**
 *@In cấu trúc các phần tử trong mảng
 */
echo "<pre>";
print_r($arr);
echo "</pre>";
?>
<h2>Mảng số nguyên(Mảng liên tục)</h2>
<?php
$mang_so_nguyen = array('item 1', 'item 2', 'item 3');
echo "Item[0]: " . $mang_so_nguyen[0];
?>
<p>In mảng số nguyên</p>
<?php
for ($i = 0; $i < count($mang_so_nguyen); $i++) {
    echo $mang_so_nguyen[$i] . "<br/>";
}
?>
<p>In mảng bằng foreach</p>
<?php
foreach ($mang_so_nguyen as $key => $value) {
    echo $key . " => " . $value . "<br/>";
}
echo "<p>Hoặc</p>";
foreach ($mang_so_nguyen as $item) {
    echo $item . "<br/>";
}
?>

<h2>Mảng kết hợp</h2>
<p>Cấu trúc mảng kết hợp</p>
<?php
// $mang_ket_hop = array();
// $mang_ket_hop['key1'] = 'item 1';
// $mang_ket_hop['key2'] = 'item 2';
// $mang_ket_hop[] = 'item 3';
// $mang_ket_hop[] = 'item 4';
$mang_ket_hop = array('key 1' => 'item 1', 'key 2' => 'item 2', 0 => 'item 3', 1 => 'item 4');
echo "<pre>";
print_r($mang_ket_hop);
echo "</pre>";
?>
<p>Truy cập mảng kết hợp</p>
<?php
foreach ($mang_ket_hop as $key => $value) {
    echo $value;
}
?>
<h2>Mảng đa chiều</h2>
<!-- Biểu diễn json dưới dạng mảng đa chiều -->
<!-- sinh_vien:[{
  mssv:123,
  ten: a,
  lop:1,
  diem:[4,5,6]
},
{
  mssv:234,
  ten: b,
  lop:2,
  diem:[4,5,6]
},
{
  mssv:456,
  ten: c,
  lop:3,
  diem:[4,5,6]
}] -->

<?php
$sinhvien = array(
    "123" => array('ten' => 'a', 'lop' => '1', 'diem' => array(4, 5, 6)),
    "234" => array('ten' => 'b', 'lop' => '2', 'diem' => array(6, 7, 8)),
    "456" => array('ten' => 'c', 'lop' => '3', 'diem' => array(5, 8, 9)),
);
echo "<pre>";
print_r($sinhvien);
echo "</pre>";
?>
<p>Lấy tên sinh viên có mssv 123</p>
<?php
echo $sinhvien['123']['ten'];
?>
<p>Lấy điểm môn thứ 2 của mssv 234</p>
<?php
echo $sinhvien['123']['diem'][1];
?>
<p>Cập nhật tên của mssv 456 thành 'sinhviengioi'</p>
<?php
$sinhvien['456']['ten'] = 'sinhviengioi';
echo "<pre>";
print_r($sinhvien);
echo "</pre>";
?>
<p>In tổng điểm của các sinh viên</p>
<?php
foreach ($sinhvien as $key => $value) {
    echo $value['ten'] . ": " . array_sum($value['diem']) . "<br/>";
}
?>

<p>array_values và array_keys</p>
<?php
$mang_values = array_values($mang_ket_hop);
$mang_keys = array_keys($mang_ket_hop);
echo "Mang Values <br/>";
echo "<pre>";
print_r($mang_values);
echo "</pre>";
echo "Mang Keys <br/>";
echo "<pre>";
print_r($mang_keys);
echo "</pre>";
?>