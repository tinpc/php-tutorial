## Filter
- Validate để kiểm xác nhận hoặc kiểm tra xem dữ liệu có đáp ứng đủ điều kiện yêu cầu hay không
- Filter dùng để lọc các dữ liệu đầu vào không phù hợp và trả về một kết quả đúng với yêu cầu
-Sử dụng hàm filter_var($variable,$filter,$option) để kiểm tra biến $variable phù hợp với điều kiện $filter. $filter thường có các giá trị
  - FILTER_VALIDATE_BOOLEAN
  - FILTER_VALIDATE_EMAIL
  - FILTER_VALIDATE_FLOAT
  - FILTER_VALIDATE_IP
  - FILTER_VALIDATE_URL...
- Sử dụng như filter bằng cách: filter_var($variable,FILTER_CALLBACK,$option)
  - $option = array('options'=>'checkNumber')
  - xây dựng hàm checkNumber làm gì đó nếu dữ liệu đưa vào không đúng.
- filter_var($variable,FILTER_VALIDATE_REGEXP,$option) để sử dụng regex

## Error - Exception
- Câu lệnh die($variable) in ra nội dung của biến $variable và kết thúc chương trình tại vị trí gọi lệnh die
- Tắt hiển thị lỗi trên trình duyệt và ghi lỗi vào file log
  - ini_set('display_errors', 'off');
  - ini_set('log_errors', 'on');
  - ini_set('error_log', 'php-error.log'); php-error.log là tên file ghi log tùy ý
- Thiết lập hiển thị lỗi bằng hàm error_reporting($value) hoặc ini_set(error_reporting,$value)
  - 0 ẩn tất cả các lỗi
  - E_ALL hiển thị tất cả các lỗi
  - E_WARNING
  - E_NOTICE
  - Giá trị được thiết lập mặc định là (E_ALL ^ E_NOTICE) 
- error_function($error_level, $error_message, $error_file, $error_line, $error_context) để xây dựng lại nội dung thông báo muốn hiển thị
  - $error_level: E_ALL,E_WARNING,E_NOTICE...
  - $error_message: Nội dung của thông báo lỗi
  - $error_file: Tên file xảy ra lỗi
  - $error_line: Dòng xảy ra lỗi
  - $error_context: Context nào lỗi(GET, POST,SESSION, COOKIE...)
- Dùng try-catch bắt exception giống js

