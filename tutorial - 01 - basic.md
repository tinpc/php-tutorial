## Kiểu dữ liệu

- hàm gettype(\$var) trả về kiểu dữ liệu (type of), hàm var_dump show kiểu dữ liệu và độ dài.
- Chuyển đổi kiểu dữ liệu có 2 cách:
  - ép kiểu : (int)\$var
  - settype(\$var,"integer")
- Kiểm tra kiểu dữ liệu của biến is_numberic(), is_float(),is_string(), is_array(), is_object()...

## Toán tử

- / Chia lấy nguyên, % Chia lấy phần dư
- Toán tử gán: x= x+5 => x+=5
- ++x => tăng x lên 1 sau đó return; x++ return x rồi mới tăng lên 1 đơn vị. $x=1; $y = ++x => y=2; còn nếu \$y=x++ => y=1;
- So sánh khác ngoại trừ dùng != cũng có thể dùng <>
- Toán tử 3 ngôi giống js

## Form

- Để lấy giá trị từ form submit
  - get: \$GET
  - post: \$POST
  - \$\_REQUEST được dùng chung cho cả get và post
  - Dùng hàm isset(\$var) để kiểm tra một phần tử có tồn tại không)
  - Khi không cần chuyển đến trang khác mà cần lấy giá trị trên trang hiện tại thì action="#"
  - có thể set giá trị hiển thị value="<?php echo $var ?>"

## Function

- Để import file khác có 2 cách:
  - require: Nếu trong file require xảy ra lỗi thì ngừng thực hiện chương trình
  - include: Vẫn thực hiện tiếp chương trình nếu có lỗi\
- Function trên trang nên được đặt trong function.php
- include_once và require_once sẽ kiểm tra file đã được import chưa, nếu đã có thì không import lại thêm nữa
- Muốn truy cập biến global bên trong hàm thì trong thân hàm phải khai báo lại giá trị đó trước từ khóa global hoặc \$GLOBAL["ten_bien"]
- Biến của hàm truyền theo kiểu tham trị thì giá trị không đổi sau khi hàm thực hiện
- Biến của hàm truyền theo kiểu tham chiếu thì giá trị sẽ cập nhật lại theo nôi dụng hàm. Biến tham chiếu khai báo ở function bằng &\$var

## ASNT (Number, String, Array, Time)

### Array

- Trước khi duyệt mảng nên kiểm tra mảng có rộng không để tránh lỗi.
- Mảng số nguyên (còn gọi là mảng liên tục) là mảng mà các chỉ số phần tử (index) phải là kiểu số nguyên.
- Mảng kết hợp là mảng mà các chỉ số index có thể là chuỗi hoặc số. In nội dung mảng kết hợp chỉ có thể dùng foreach
- Mảng đa chiều là mảng mà mỗi phần tử có thể là 1 mảng... còn gọi là mảng lồng. In nội dung mảng lồng cũng chỉ có thể dùng foreach
- array_values($array) trả về mảng liên tục chứa các value của mảng $array
- array_keys($array) trả về mảng liên tục chứa các key của mảng $array
- array_pop($array) lấy ra phần tử cuối cùng của mảng, array_shift($array) lấy ra phần tử đầu tiên của mảng
- array_unique($array) sẽ loại bỏ những phần tử trùng nhau trong mảng $array
- unset($array[index]) để xóa phần tử vị trí thứ index của mảng, unset($array[index1],$array[index2],...) hoặc unset($array) để xóa cả mảng.
- array_push($array,$val1,\$val2...) thêm phần tử vào cuối mảng, trả về giá trị kiểu số nguyên là độ dài mảng mới
- array_unshift($array,$val1,\$val2...) thêm phần tử vào đầu mảng, trả về giá trị kiểu số nguyên là độ dài mảng mới
- array_reverse(\$array) đảo ngược mảng và trả về mảng mới
- array_flip(\$array) hoán đổi key và value của từng phần tử, trả về mảng mới
- array_sum($array) tính tổng mảng, min($array), max(\$array) lấy giá trị nhỏ nhất và lớn nhất
- array_count_values(\$array) thống kê sự xuất hiện các phần tử trong mảng. Trả về mảng chứa [value] => [count]
- array_merge($array1,$array2...) nhập nhiều mảng lại trả về 1 mảng mới
- array_rand($array,$number) random $number keys của mảng $array, trả về mảng số nguyên chứa các keys
- array_search($value, $array) tìm \$value trong mảng, trả về key của giá trị tìm được
- array_key_exists($key,$array) kiểm tra $key có tồn tại trong mảng $array không. Nếu có trả về true
- in_array($value,$array) kiểm tra $value có tồn tại trong mảng $array không. Nếu có trả về true
- array_change_key_case($array,case) chuyển $key trong mảng thành chữ hoa hoặc thường. Trả về 1 mảng mới. CASE_UPPER => chữ hoa, default case = CASE_LOWER
- implode($str,$array) chuyển các $value của mảng thành chuỗi cách nhau ký tự $str(join)
- explode($delimiter,$str) chuyển 1 chuỗi thành 1 mảng, tách chuỗi dựa vào \$delimiter(split)
- curent($array),end($array),next($array),prev($array), reset(\$array) truy xuất đến vị trí của mảng
  - giá trị current của mảng nếu không được chỉ định thì nó sẽ trỏ tới vị trí đầu tiên của mảng
  - khi reset thì current sẽ là vị trí đầu tiên của mảng
- serialize($value) chuyển đổi $value có kiểu string/array/object thành chuỗi đặc biệt để lưu vào database, unserialize(\$value) sẽ dịch ngược lại chuỗi về ban đầu.
- shuffle(\$array) xáo trộn các phần tử của mảng số nguyên. Trả về một mảng mới.
- compact() tạo mảng mới từ các biến có sẵn.
  - $name = "nguyen van a"; $age = 18;
  - compact("name","age");
  - output Array([name]=>"nguyen van a" [age]=>18)
- range($start, $end, $increate) tạo ra một mảng liên tục trong khoản $start tới $end, mỗi lần tăng $increate.
- array_combine($keys, $values) tạo một mảng mới có key được lấy từ mảng $keys và value được lấy từ mảng $values theo tuần tự.
- Các hàm so sánh mảng khác nhau:
  - array_diff($array1, $array2) trả về mảng bao gồm các phần tử có $value tồn tại trong $array1 nhưng không tồn tại trong \$array2
  - array_diff_key($array1, $array2) trả về mảng bao gồm các phần tử có $key tồn tại trong $array1 nhưng không tồn tại trong \$array2
  - array_diff_assoc($array1, $array2) trả về mảng bao gồm các phần tử có cả $key và $value tồn tại trong $array1 nhưng không tồn tại trong $array2
- Các hàm so sánh mảng giống nhau:
  - array_intersect($array1, $array2) trả về mảng bao gồm các phần tử có \$value giống nhau giữa 2 mảng
  - array_intersect_key($array1, $array2) trả về mảng bao gồm các phần tử có \$key giống nhau giữa 2 mảng
  - array_intersect_assoc($array1, $array2) trả về mảng bao gồm các phần tử có cả $key và $value giống nhau giữa 2 mảng
- array_walk($array, $function_name) sẽ gửi các giá trị của mảng đến hàm nào đó($key, $value là đối số của hàm) để xử lý và nhận về kết quả là một mảng mới (map).
- array_map($function_name, $array1, \$array2) gửi giá trị của một hoặc nhiều mảng đến 1 hàm(số đối số hàm sẽ bằng với số mảng) để xử lý và nhận về kết quả là một mảng mới.
- array_slice($array, $offset, $length, $preserve) trích xuất một đoạn phần tử của mảng từ vị trí bắt đầu $offset và lấy $length phần tử.
  - $length null thì lấy từ $offset đến cuối mảng
  - \$length 0 trả về mảng rỗng
  - $preserve default false, true sẽ trả về mảng mới mà các phần tử có $key đúng bằng \$key trong mảng cũ
- array_splice($arra1, $offset, $length, $array2) xóa bỏ $length phần tử trong mảng $array1 tính từ vị trí $offset sau đó thay thế các phần tử bị loại bỏ bằng mảng $array2
- Các hàm sắp xếp mảng:
  - sort($array) sắp xếp mảng tăng dần theo $value
  - rsort($array) sắp xếp mảng giảm dần theo $value
  - ksort($array) sắp xếp mảng giảm dần theo $key
  - krsort($array) sắp xếp mảng giảm dần theo $key

### String

- Hiển thị ' " bằng cách \' \"
- Nối chuỗi bằng ký tự "."
- strlen($string) lấy chiều dài chuỗi $string, đối với chuỗi có ký tự utf-8 thì phải dùng hàm mb_strlen(\$utf8_string,"UTF-8")
- str_word_count(\$string) đếm số từ xuất hiện trong chuỗi
- Các hàm chuyển đổi string:
  - strtoupper($string)|strtolower($string) chuyển thành chữ hoa/thường.
  - ucfirst(\$string)|lcfirst ký tự đầu tiên của chuỗi thành chữ hoa/thường.
  - ucwords(\$string) chữ cái đầu các từ trong chuỗi thành chữ hoa
- Các hàm xác định vị trí:
  - stripos($string, $sub_string) Trả về chỉ số xuất hiện đầu tiên của \$sub_string trong chuỗi
  - strripos($string, $sub_string) Trả về chỉ số xuất hiện cuối cùng của \$sub_string trong chuỗi
- strrev(\$string) đảo ngược chuỗi
- substr($string,$offset, $length) Trích xuất chuỗi từ $offset với độ dài \$length.
  - $length null thì lấy từ $offset đến hết chuỗi.
  - \$offset âm nghĩa là vị trí được đếm ngược lại tính từ cuối chuỗi.
- ltrim($string, $params) sẽ xóa các ký tự \$params bên trái của chuỗi
  - tham số \$params null sẽ xóa bỏ các ký tự sau:
    - "\0" - NULL
    - "\t" - tab
    - "\n" - new line
    - "\x0B" - vertical tab
    - "\r" - carriage return
    - "" - ordinary white space
  - \$params truyền vào có tác dụng giống hàm replace()
  - tương tự thì rtrim($string, $params) sẽ xóa các ký tự \$params bên phải của chuỗi
  - trim($string, $params) xóa bỏ ký tự \$params 2 bên chuỗi
- isset(), trim() có thể dùng để kiểm tra chuỗi có rỗng hay không.
- str_repeat($string, $n) lặp lại chuỗi n lần
- Hàm ASCII
  - chr(\$ascii_code) trả về ký tự tương ứng với mã ASCII truyền vào
  - ord(\$string) trả về mã ASCII của ký tự đầu tiên trong chuỗi
- parse_str(\$string) chuyển chuỗi có nội dung truy vấn thành biến hoặc mảng
- parse_url($url) truy xuất các thành phần protocol, domain, path... của $url. Kết quả trả lại một mảng
- strcmp($str1, $str2) so sánh 2 chuỗi với nhau, Trả về 0 nếu 2 chuỗi bằng nhau, dương nếu $str1 > $str2 và âm nếu $str1 < $str2
- substr_compare($str1, $str2, $start, $length) lấy $length phần tử từ vị trí $start trong chuỗi $str1 sau đó so sánh $str2
- str_pad($str, $length, $pad_string, $pad_type) tăng độ dài của chuỗi $tr thành $length ký tự với các ký tự thêm vào là $pad_string và cớ chế thêm là $pad_type
  - \$pad_type có các giá trị STR_PAD_LEFT, STR_PAD_RIGHT, STR_PAD_BOTH\
- str_shuffle(\$string) sắp xếp ngẫu nhiên các ký tự trong chuỗi.
- str_replace($find, $replace, $string) thay thế giá trị $find trong chuỗi bằng giá trị \$replace
- sub_count($string,$sub_string,$start,$length) lấy $length phần tử từ vị trí $start trong chuỗi $string và trả về số lần xuất hiện của $sub_string trong chuỗi vừa lấy
- str_split($string, $length) cắt chuỗi thành từng phần tử của mảng, mỗi phần tử có độ dài \$length
- Các hàm với ký tự đặc biệt ' " \
  - addslashes($string) thêm ký tự "\" vào trước các ký tự đặc biệt trong chuỗi $string
  - addcslashes($string,$character) thêm ký tự "\" vào trước ký tự $character trong chuỗi $string
  - stripslashes(\$string) hiển thị chuỗi không có các ký tự đặc biệt được tạo bởi hàm addslashes
  - stripcslashes(\$string) hiển thị chuỗi không có các ký tự đặc biệt được tạo bởi hàm addcslashes
- Các hàm với HTML entity:
  - htmlspecialchars(\$string) chuyển các giá trị được quy định trước sang html entity
  - htmlspecialchars_decode(\$string) decode hàm htmlspecialchars
  - htmlentities(\$string) chuyển đổi các ký tự sang html entity
  - html_entity_decode(\$string) decode hàm htmlentities
  - get_html_translation_table() xem danh sách các giá trị html entity
  - strip_tags(\$string) loại bỏ các thẻ html có trong chuỗi

## Number

- Hàm làm tròn số thập phân
  - round($number,$length) làm tròn đến số nguyên gần nhất. round(8.2345678,3) = 8.234
  - ceil(\$number) làm tròn đến số nguyên gần nhất và lớn nhất
  - floor() làm tròn đến số nguyên gần nhất và nhỏ nhất
- rand($min,$max) trả về một giá trị ngẫu nhiên nằm trong đoạn [min, max]
- number_format($number,$length) định dạng hiển thị phần ngàn của $number với $length số thập phân
- Các hàm tính toán số học:
  - abs(\$number) trả về giá trị tuyệt đối của một số
  - pow($x, $y) trả về lũy thừa x mũ y
  - sqrt($number) tính căn bậc hai của $number
  - logarithm: log(), log10(),log1p()
  - hàm lượng giác: sin(), cos(), tan(), rad2deg(), pi()

## Time

- Các hàm về thời gian:
  - getdate() lấy thời gian của server
  - date_default_timezone_get() trả về kết quả múi giờ được thiết lập sẵn
  - date_default_timezone_set() thiết lập múi giờ
  - timezone_identifiers_list() xem danh sách múi giờ
- Xác định khoảng thời gian hiện tại so với timestamp
  - Unix Timestamp là dạng thời gian thường được dùng trên hệ thống Unix. Là số giây được tính từ thời điểm 00:00:00 giờ ngày 01/01/1970 theo giờ GMT
  - time() trả về số giây từ thời điểm hiệnt tại so với timestamp (new Date())
  - mktime($hour,$min,$sencond,$month,$day,$year) trả về số giây tại một thời điểm nào đó so với timestamp
  - date($format,$time) hiển thị $time(có định dạng timestamp) theo định dạng $format
    - d => 01->31 (day)
    - j => 1->31 (day)
    - m => 01->12 (month)
    - n => 1->12 (month)
    - M => Jan,Feb,... (month)
    - Y => 2020 (year)
    - y => 20 (year)
    - g => 1->12 (hour)
    - G => 0->23 (hour)
    - h => 01->12 (hour)
    - H => 01->23 (hour)
    - i => 0->59 (min)
    - s => 00->59 (second)
    - a => am pm
    - A => AM PM
    - sử dụng các ký tự đại diện trên để tạo ra \$format theo định dang mong muốn
- checkdate($month, $day,\$year) kiểm tra các giá trị truyền vào có tạo thành ngày hợp lệ không
- sử dụng jQuery UI Datepicker để tạo ngày(đọc api jQuery UI Datepicker)

## Làm việc với dữ liệu json
 - json_decode($json_string, $assoc) chuyển một chuỗi JSON sang dạng mảng hoặc object.
  - $json_string: là chuỗi JSON
  - $assoc có hai giá trị true / false. Nếu true thì kết quả nó trả về là dạng  array, ngược lại nếu false thì kết quả trả về dạng object. Mặc định là false
- json_encode($array) chuyển một mảng trong PHP hoặc object trong PHP thành chuỗi JSON
