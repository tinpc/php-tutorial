<?php
session_start();
$variable = 'This is astring';
$mang = array(
    array('key1' => "a", "key2" => "b"),
    array('key1' => "c", "key2" => "d"),
);
function hello()
{
    echo "Hello!";
};
$_SESSION['variable'] = $variable;
$_SESSION['array'] = $mang;
$_SESSION['function'] = '<?php
function hello() {
  echo "Hello!";
};
?>';
echo "<pre>";
print_r($_SESSION);
echo "</pre>";

/**
 *@Gọi hàm lưu trong session
 *  eval('?>' . $_SESSION['function']);
 *  echo hello();
 */

session_unset();

echo "<pre>";
print_r($_SESSION);
echo "</pre>";
